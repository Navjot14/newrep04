namespace UserReservation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("reserve")]
    public partial class reserve
    {
        [Key]
        public int RId { get; set; }
        [DataType(DataType.Date)]
        [Column("check in date")]
        [StringLength(50)]
        public string check_in_date { get; set; }
        [DataType(DataType.Date)]
        [Column("check out date")]
        [StringLength(50)]
        public string check_out_date { get; set; }

        [Column("Number of Guests")]
        [StringLength(50)]
        public string Number_of_Guests { get; set; }

        [Column("Number of rooms")]
        [StringLength(50)]
        public string Number_of_rooms { get; set; }

        [Column("credit card")]
        [StringLength(50)]
        public string credit_card { get; set; }

        [Column("name on credit card")]
        [StringLength(50)]
        public string name_on_credit_card { get; set; }

        [Column("credit card number")]
        [StringLength(50)]
        [credittype]
        public string credit_card_number { get; set; }

        [Column("Expiration Date")]
        [StringLength(50)]
        public string Expiration_Date { get; set; }

        [StringLength(50)]
        public string Username { get; set; }
    }
}
